import axios, { AxiosResponse } from 'axios';
import { APINewsItem } from './DTO/APINewsItem.js';
import clientConfig from '../config.json';

export async function getLatestNewsFromAPI(): Promise<APINewsItem[]> {

    const apiResponse = await getFromAPI();
    const articles: [] = apiResponse.data;

    const dtos: APINewsItem[] = [];

    articles.forEach(article => {
        dtos.push(APINewsItem.fromJSON(article));
    })

    return dtos;
}

function getFromAPI(): Promise<AxiosResponse> {
    return axios.get(clientConfig.err.api);
}
