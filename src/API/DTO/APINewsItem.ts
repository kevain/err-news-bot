import { APINewsPhoto } from "./APINewsPhoto.js";

export class APINewsItem {
    id: number;
    heading: string;
    lead: string;
    canonicalUrl: string;
    primaryCategoryName: string;
    published: number;
    photos?: APINewsPhoto[];

    constructor(id: number, heading: string, lead: string, canonicalUrl: string, primaryCategoryName: string, published: number, photos: APINewsPhoto[]) {
        this.id = id;
        this.heading = heading;
        this.lead = lead;
        this.canonicalUrl = canonicalUrl;
        this.primaryCategoryName = primaryCategoryName;
        this.published = published;
        this.photos = photos;
    }

    static fromJSON(json: any): APINewsItem {
        const id = +json['id'];
        const heading = json['heading'];
        const lead = json['lead'];
        const canonicalUrl = json['canonicalUrl'];
        const primaryCategoryName = json['primaryCategory']['name']
        const published = json['updated']
        const photos = json['photos']?.map((photo: any) => APINewsPhoto.fromJson(photo));

        return new APINewsItem(id, heading, lead, canonicalUrl, primaryCategoryName, published, photos);
    }
}