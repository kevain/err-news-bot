export class APINewsPhoto {
    id: number;
    photoUrlOriginal: string;

    constructor(id: number, photoUrlOriginal: string) {
        this.id = id;
        this.photoUrlOriginal = photoUrlOriginal;
    }

    static fromJson(json: any): APINewsPhoto {
        return new APINewsPhoto(
            json['id'],
            json['photoUrlOriginal']
        );
    }

}