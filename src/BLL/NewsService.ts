import { APINewsItem } from "../API/DTO/APINewsItem.js";
import { getLatestNewsFromAPI } from "../API/ERRAPIService.js";
import { Article } from "../Domain/Article.js";

export async function getMostRecentNewsArticle(): Promise<Article> {
    const articles = await getLatestNewsFromAPI();
    const mostRecentArticle = articles[0];

    return mapApiArticleToDomainEntity(mostRecentArticle);
}

function mapApiArticleToDomainEntity(inObject: APINewsItem): Article {
    return new Article(inObject.id,
        inObject.heading,
        inObject.lead.replace(/<\/?p>/g, ''), // For some reason API provides intro text with paragraph HTML tags.
        inObject.canonicalUrl,
        inObject.primaryCategoryName,
        new Date(inObject.published * 1000),
        (inObject.photos && inObject.photos.length > 0) ? inObject.photos[0].photoUrlOriginal : null
    );
};