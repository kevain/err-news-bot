export class Article {
    id: number;
    title: string;
    intro: string;
    url: string;
    category: string;
    published: Date;
    photoUrl?: string | null;

    constructor(id: number, title: string, intro: string, url: string, category: string, published: Date, photoUrl: string | null) {
        this.id = id;
        this.title = title;
        this.intro = intro;
        this.url = url;
        this.category = category;
        this.published = published;
        this.photoUrl = photoUrl;
    }
}