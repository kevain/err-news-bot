import * as Discord from "discord.js";
import { getMostRecentNewsArticle } from "./BLL/NewsService.js";
import { Article } from "./Domain/Article.js";

import clientConfig from './config.json';

const client = new Discord.Client();
let newsChannel: Discord.TextChannel;

let lastNews: Article;

async function CheckForNews(): Promise<void> {
    const news = (await getMostRecentNewsArticle()
        .catch(e => console.error(e)));

    if (news != null && (lastNews == null || news.id != lastNews.id)) {
        postNews(news);
        lastNews = news;
    }
}

async function fetchNewsChannel(): Promise<void> {
    try {
        newsChannel = await client.channels.fetch(clientConfig.discord.newsChannelId) as Discord.TextChannel;
        console.log(`Found news channel: #${newsChannel.name}`)
    } catch (error) {
        throw new Error(`Unable to obtain the provided channel.\n${error.message}`);
    }
}

function postNews(news: Article): void {
    const response = new Discord.MessageEmbed();

    response.setTitle(news.title);
    response.setAuthor(news.category);
    response.setDescription(news.intro);
    response.setURL(news.url);
    response.setColor('#0453a3')
    response.setTimestamp(news.published);

    if (news.photoUrl) {
        response.setThumbnail(news.photoUrl);
    }

    newsChannel.send(response);
}

client.once('ready', async () => {
    console.log(`Connected to Discord as ${client.user?.username}`);

    fetchNewsChannel()
        .then(() => {
            console.log('Keeping an eye on the news.');
            CheckForNews();
            setInterval(CheckForNews, clientConfig.err.frequency);
        })
        .catch((err) => {
            console.error(err.message);
            client.destroy();
        })
});


client.login(clientConfig.discord.authenticationToken)
    .catch((error) => {
        console.error(`Failed establishing connection with Discord.\n${error.message}`)
    });
