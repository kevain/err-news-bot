# ERR News Discord bot
A basic Discord bot, which checks for latest news from Estonian public broadcasting (ERR) and posts them in the Discord chat.

## Used technologies
1. NodeJS
2. TypeScript

## Requirements
Node, npm, TS compiler

## Setup 
1. Clone this repository.
2. Install dependencies.
3. Add your own Discord API token (_Get it from their developer portal_) and news channel ID to the ./src/config.json file. 
3. Compile TypeScript code.
4. Run

## Screenshots
![Example of bot's feed](./screenshots/screenshot-feed.jpg)

## Roadmap 
Ideas and thoughts, how to take this further. In no particular order:

1. Improve architecture (Finish BLL layer, ...). 
2. Integrate other news sources?
3. Allow people to invite the bot to their own servers and assign their own feed channel. This would require a datastore to store info about all the feed channels. 

